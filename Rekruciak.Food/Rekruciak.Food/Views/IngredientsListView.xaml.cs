﻿using ITA.WPF_ReuseableUserControls.FiltrableList.Services;
using Rekruciak.Food.Interfaces.BO.Ingredients;
using Rekruciak.Food.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rekruciak.Food.UI.Views
{
    /// <summary>
    /// Interaction logic for IngredientsListView.xaml
    /// </summary>
    public partial class IngredientsListView : UserControl
    {
        FilterService _filterService = new FilterService();
        public IngredientsListView(IngredientsListViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            Loaded += ((s, e) =>
            {
                FilterTextBox.Focus();
            });
        }
        private void FilterTextChanged(object sender, TextChangedEventArgs e)
        {
            _filterService.SetValueToFilterBy((sender as TextBox).Text);
            CollectionViewSource.GetDefaultView(MainListView.ItemsSource).Refresh();
        }

        private void MainListView_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            var view = CollectionViewSource.GetDefaultView((sender as ListView).ItemsSource);
            if (view != null)
            {
                _filterService.SetPropertiesToFilterBy(sender as ItemsControl);
                view.Filter = _filterService.FrameworkFilter;
            }
        }
    }
}
