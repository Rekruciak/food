﻿using Rekruciak.Food.Interfaces.BO;
using Rekruciak.Food.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Rekruciak.Food.UI.Views
{
    /// <summary>
    /// Interaction logic for RecipeDetailsView.xaml
    /// </summary>
    public partial class RecipeDetailsView : UserControl
    {
        public RecipeDetailsView(RecipeViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            viewModel.View = this;
            Loaded += ((s, e) =>
            {
                NameTB.Focus();
                ((INotifyCollectionChanged)PreparationStepsListView.Items).CollectionChanged += PreparationStepsListView_CollectionChanged;
                ((INotifyCollectionChanged)IngredientsListView.Items).CollectionChanged += IngredientsListView_CollectionChanged;
            });
            FocusableChanged += RecipeDetailsView_FocusableChanged;
        }

        private void RecipeDetailsView_FocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        #region Ingredients auto focus
        int ingredientSelectedIndex = -1;
        private void IngredientsListView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                ingredientSelectedIndex = IngredientsListView.Items.IndexOf(e.NewItems[0]);
                IngredientsListView.SelectedIndex = ingredientSelectedIndex;
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove && IngredientsListView.Items.Count > 0)
            {
                FocusLastIngredient();
            }
        }
        private void Bw_ingredientsAutoFocuser_DoWork(object sender, DoWorkEventArgs e)
        {
            Bw_AutoFocuser_DoWork(IngredientsListView, "IngredientAmountTextBox");
        }


        private void IngredientsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BackgroundWorker bw_ingredientsAutoFocuser = new BackgroundWorker();
            bw_ingredientsAutoFocuser.DoWork += Bw_ingredientsAutoFocuser_DoWork;
            bw_ingredientsAutoFocuser.RunWorkerAsync();
        }
        #endregion

        #region Preparation steps auto focus 
        int preparationStepSelectedIndex = -1;
        private void PreparationStepsListView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                preparationStepSelectedIndex = PreparationStepsListView.Items.IndexOf(e.NewItems[0]);
                PreparationStepsListView.SelectedIndex = preparationStepSelectedIndex;
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove && IngredientsListView.Items.Count > 0)
            {
                FocusLastPreparationStep();
            }
        }

        private void Bw_preparationStepsAutoFocuser_DoWork(object sender, DoWorkEventArgs e)
        {
            Bw_AutoFocuser_DoWork(PreparationStepsListView, "DescriptionTextBox");
        }


        private void PreparationStepsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BackgroundWorker bw_preparationStepsAutoFocuser = new BackgroundWorker();
            bw_preparationStepsAutoFocuser.DoWork += Bw_preparationStepsAutoFocuser_DoWork;
            bw_preparationStepsAutoFocuser.RunWorkerAsync();
        }
        #endregion

        #region Auto focus helpers 
        private void Bw_AutoFocuser_DoWork(ListView listViewToFocus, string textboxNameToFocus)
        {
            Thread.Sleep(100);
            Dispatcher.Invoke(new Action(() =>
            {
                if (listViewToFocus.SelectedIndex == -1)
                    return;
                ItemContainerGenerator generator = listViewToFocus.ItemContainerGenerator;
                ListViewItem selectedItem = (ListViewItem)generator.ContainerFromIndex(listViewToFocus.SelectedIndex);
                TextBox tbFind = GetDescendantByType(selectedItem, typeof(TextBox), textboxNameToFocus) as TextBox;
                if (tbFind != null)
                {
                    FocusHelper.Focus(tbFind);
                    listViewToFocus.UnselectAll();
                }
            }));
        }

        public static class FocusHelper
        {
            public static void Focus(UIElement element)
            {
                element.Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(delegate ()
                {
                    element.Focus();
                }));
            }
        }
        
        public static Visual GetDescendantByType(Visual element, Type type, string name)
        {
            if (element == null)
                return null;

            if (element.GetType() == type &&
                element is FrameworkElement fe &&
                fe.Name == name)
                return fe;

            Visual foundElement = null;
            if (element is FrameworkElement)
                (element as FrameworkElement).ApplyTemplate();
            for (int i = 0;
                i < VisualTreeHelper.GetChildrenCount(element); i++)
            {
                Visual visual = VisualTreeHelper.GetChild(element, i) as Visual;
                foundElement = GetDescendantByType(visual, type, name);
                if (foundElement != null)
                    break;
            }
            return foundElement;
        }       
        #endregion

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            bool approvedDecimalPoint = false;
            char a = Convert.ToChar(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            if (e.Text == a.ToString())
            {
                if (!((TextBox)sender).Text.Contains(a))
                    approvedDecimalPoint = true;
            }

            if (!(char.IsDigit(e.Text, e.Text.Length - 1) || approvedDecimalPoint))
                e.Handled = true;
        }
        
        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).SelectAll();
        }

        private void FocusLastIngredient()
        {
            ingredientSelectedIndex = IngredientsListView.Items.Count - 1;
            IngredientsListView.SelectedIndex = ingredientSelectedIndex;
        }

        private void FocusLastIngredientOrCreateNew()
        {
            if (IngredientsListView.Items.Count != 0)
            {
                FocusLastIngredient();
            }
            else if ((bool)AddNewIngredientButton.Command?.CanExecute(null))
            {
                AddNewIngredientButton.Command.Execute(null);
            }
        }

        private void FocusLastPreparationStep()
        {
            preparationStepSelectedIndex = PreparationStepsListView.Items.Count - 1;
            PreparationStepsListView.SelectedIndex = preparationStepSelectedIndex;
        }

        private void FocusLastPreparationStepOrCreateNew()
        {
            if (PreparationStepsListView.Items.Count != 0)
            {
                FocusLastPreparationStep();
            }
            else if ((bool)AddNewPreparationStepButton.Command?.CanExecute(null))
            {
                AddNewPreparationStepButton.Command.Execute(null);
            }
        }

        private void ListaSkladnikowGrid_GotFocus(object sender, RoutedEventArgs e)
        {
            FocusLastIngredient();
        }

        private void ListaKrokowGrid_GotFocus(object sender, RoutedEventArgs e)
        {
            FocusLastPreparationStep();
        }

        private void NameTB_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                e.Handled = true;
                if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    FocusLastPreparationStepOrCreateNew();
                else
                    FocusLastIngredientOrCreateNew();
            }
            if (e.Key == Key.Enter && !Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
            {
                e.Handled = true;
                FocusLastIngredientOrCreateNew();
            }
        }

        private void IngredientAmountTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                e.Handled = true;
                if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    NameTB.Focus();
                else
                    FocusLastPreparationStepOrCreateNew();

            }
        }

        private void DescriptionTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                e.Handled = true;
                if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    FocusLastIngredientOrCreateNew();
                else
                    NameTB.Focus();
            }
        }

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
            {
                e.Handled = true;
                SaveAndExitButton.Command.Execute(null);
            }
        }
    }
}
