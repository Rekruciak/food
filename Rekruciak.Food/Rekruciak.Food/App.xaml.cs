﻿using Rekruciak.Food.UI.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF_ThirdParty.Windows;

namespace Rekruciak.Food.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            BaseWindowWithChangableContent wind = new BasicWindow("Version for presentation");
            wind.Width = 1200;
            wind.Height = 800;
            wind.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            ITA.WPF_ThirdParty.OnStartup.StartApplication(new AllRecipesView(), wind);
        }
    }
}
