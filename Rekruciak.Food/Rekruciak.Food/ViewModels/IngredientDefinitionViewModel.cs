﻿using Rekruciak.Food.Interfaces.BO.Ingredients;
using Rekruciak.Food.UI.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF_ThirdParty;
using WPF_ThirdParty.VM_Implementation;
using WPF_ThirdParty.Windows;

namespace Rekruciak.Food.UI.ViewModels
{
    public class IngredientDefinitionViewModel : BaseViewModel
    {
        private IIngredientBase _returnValue;
        private ShowDialogWindow _window;
        private IngredientDefinitionView _view;

        public IngredientDefinitionViewModel()
        {
            CountableIngredient = ApplicationViewModel.Instance.BLC.CreateEmptyIngredient<ICountableIngredient>() as ICountableIngredient;
            StableIngredient = ApplicationViewModel.Instance.BLC.CreateEmptyIngredient<IStableIngredient>() as IStableIngredient;
            LiquidIngredient = ApplicationViewModel.Instance.BLC.CreateEmptyIngredient<ILiquidIngredient>() as ILiquidIngredient;
        }

        public ICountableIngredient CountableIngredient { get; set; }
        public IStableIngredient StableIngredient { get; set; }
        public ILiquidIngredient LiquidIngredient { get; set; }

        private IIngredientBase _ingredientDefinitionBeingDisplayed;
        public IIngredientBase IngredientDefinitionBeingDisplayed
        {
            get => _ingredientDefinitionBeingDisplayed;
            set
            {
                _ingredientDefinitionBeingDisplayed = value;
                OnPropertyChanged(nameof(IngredientDefinitionBeingDisplayed));
            }
        }

        private bool _isTypeCountableIngredient;
        public bool IsTypeCountableIngredient
        {
            get => _isTypeCountableIngredient;
            set
            {
                if (value)
                {
                    IsTypeStableIngredient = false;
                    IsTypeLiquidIngredient = false;
                    IngredientDefinitionBeingDisplayed = CountableIngredient;
                }
                _isTypeCountableIngredient = value;
                OnPropertyChanged(nameof(IsTypeCountableIngredient));
            }
        }

        private bool _isTypeStableIngredient;
        public bool IsTypeStableIngredient
        {
            get => _isTypeStableIngredient;
            set
            {
                if (value)
                {
                    IsTypeLiquidIngredient = false;
                    IsTypeCountableIngredient = false;
                    IngredientDefinitionBeingDisplayed = StableIngredient;
                }
                _isTypeStableIngredient = value;
                OnPropertyChanged(nameof(IsTypeStableIngredient));
            }
        }

        private bool _isTypeLiquidIngredient;
        public bool IsTypeLiquidIngredient
        {
            get => _isTypeLiquidIngredient;
            set
            {
                if (value)
                {
                    IsTypeStableIngredient = false;
                    IsTypeCountableIngredient = false;
                    IngredientDefinitionBeingDisplayed = LiquidIngredient;
                }
                _isTypeLiquidIngredient = value;
                OnPropertyChanged(nameof(IsTypeLiquidIngredient));
            }
        }


        private static IIngredientBase ShowViewShowDialog(IngredientDefinitionViewModel viewModel)
        {
            viewModel._view = new IngredientDefinitionView(viewModel);
            viewModel._window = new ShowDialogWindow();
            viewModel._window.SetWindowContent(viewModel._view);
            viewModel._window.ShowDialog();
            return viewModel._returnValue;
        }

        public static IIngredientBase AddNewIngredientDefinition(string newIngredientName)
        {
            IngredientDefinitionViewModel viewModel = new IngredientDefinitionViewModel();
            viewModel.CountableIngredient.Name = newIngredientName;
            viewModel.LiquidIngredient.Name = newIngredientName;
            viewModel.StableIngredient.Name = newIngredientName;
            return ShowViewShowDialog(viewModel);
        }

        public static IIngredientBase EditIngredientDefinition(IIngredientBase ingredientDefinition)
        {
            IngredientDefinitionViewModel viewModel = new IngredientDefinitionViewModel();
            viewModel._returnValue = ApplicationViewModel.Instance.BLC.CloneIngredientByValue(ingredientDefinition);
            if (ingredientDefinition is ICountableIngredient)
            {
                viewModel.CountableIngredient = ingredientDefinition as ICountableIngredient;
                viewModel.IsTypeCountableIngredient = true;
            }
            if (ingredientDefinition is IStableIngredient)
            {
                viewModel.StableIngredient = ingredientDefinition as IStableIngredient;
                viewModel.IsTypeStableIngredient = true;
            }
            if (ingredientDefinition is ILiquidIngredient)
            {
                viewModel.LiquidIngredient = ingredientDefinition as ILiquidIngredient;
                viewModel.IsTypeLiquidIngredient = true;
            }
            return ShowViewShowDialog(viewModel);
        }
        
        public ICommand SaveAndExitCommand
        {
            get
            {
                return new RelayCommand(
                    () =>
                    {
                        _returnValue = IngredientDefinitionBeingDisplayed;
                        ApplicationViewModel.Instance.BLC.SaveIngredientDefinition(_returnValue);
                        _window.Close();
                    },
                    () =>
                        IngredientDefinitionBeingDisplayed != null
                        ? IngredientDefinitionBeingDisplayed.Name != null
                            ? IngredientDefinitionBeingDisplayed.Name.Any()
                            : false
                        : false
                    );
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    _window.Close();
                });
            }
        }
    }
}
