﻿using ITA.WPF_ThirdParty;
using ITA.WPF_ThirdParty.ShowDialogs;
using Rekruciak.Food.Interfaces.BO;
using Rekruciak.Food.UI.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF_ThirdParty;
using WPF_ThirdParty.VM_Implementation;

namespace Rekruciak.Food.UI.ViewModels
{
    public class AllRecipesViewModel : BaseViewModel
    {
        AllRecipesView _view;
        public AllRecipesViewModel(AllRecipesView view)
        {
            GetRecipes();
            _view = view;
        }

        #region Entities
        private ObservableCollection<IRecipe> _recipes;
        public ObservableCollection<IRecipe> Recipes
        {
            get => _recipes;
            set
            {
                _recipes = value;
                OnPropertyChanged(nameof(Recipes));
            }
        }

        public IRecipe SelectedRecipe
        {
            set => ScreenController.Instance.SetWindowContent(new RecipeDetailsView(new RecipeViewModel(value, false)));
        }
        #endregion

        #region Private methods
        private void GetRecipes()
        {
            Recipes = new ObservableCollection<IRecipe>(ApplicationViewModel.Instance.BLC.GetAllRecipes());
        }
        #endregion

        private void AddNewRecipe(string recipeName = "")
        {
            IRecipe recipe = ApplicationViewModel.Instance.BLC.CreateEmptyRecipe();
            if (recipe != null)
            {
                recipe.Name = recipeName;
                ScreenController.Instance.SetWindowContent(new RecipeDetailsView(new RecipeViewModel(recipe, true)));
            }
        }

        #region Commands
        public ICommand AddNewRecipeCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    AddNewRecipe();
                });
            }
        }

        public ICommand RemoveRecipeCommand
        {
            get
            {
                return new RelayCommand<IRecipe>(delegate (IRecipe recipe)
                {
                    if (MessageDialog.AskQuestion($"Usunąć przepis {recipe.Name}?"))
                    {
                        ApplicationViewModel.Instance.BLC.RemoveRecipe(recipe);
                        GetRecipes();
                    }
                });
            }
        }

        public ICommand ClickedEnterCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (_view.MainListView.Items.Count > 0)
                        SelectedRecipe = _view.MainListView.Items[0] as IRecipe;
                    else
                        AddNewRecipe(_view.FilterTextBox.Text);
                });
            }
        }
        #endregion
    }
}
