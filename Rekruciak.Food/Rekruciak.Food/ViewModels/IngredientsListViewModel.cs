﻿using ITA.WPF_ThirdParty;
using Rekruciak.Food.Interfaces.BO;
using Rekruciak.Food.Interfaces.BO.Ingredients;
using Rekruciak.Food.UI.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF_ThirdParty;
using WPF_ThirdParty.VM_Implementation;
using WPF_ThirdParty.Windows;

namespace Rekruciak.Food.UI.ViewModels
{
    public class IngredientsListViewModel : BaseViewModel
    {
        private IIngredientBase _returnValue;
        private ShowDialogWindow _window;
        private IngredientsListView _view;
        public IngredientsListViewModel()
        {
            IngredientsDefinitions = new ObservableCollection<IIngredientBase>(ApplicationViewModel.Instance.BLC.GetAllIngredientsDefinitions());
        }

        private ObservableCollection<IIngredientBase> _ingredientsDefinitions;
        public ObservableCollection<IIngredientBase> IngredientsDefinitions
        {
            get => _ingredientsDefinitions;
            set => _ingredientsDefinitions = value;
        }

        public static IIngredientBase LetUserChooseNewIngredientToRecipe()
        {
            IngredientsListViewModel viewModel = new IngredientsListViewModel();
            viewModel._view = new IngredientsListView(viewModel);
            viewModel._window = new ShowDialogWindow();
            viewModel._window.SetWindowContent(viewModel._view);
            viewModel._window.ShowDialog();
            return viewModel._returnValue;
        }

        public ICommand AnulujCommand
        {
            get
            {
                return new RelayCommand(() => _window.Close());
            }
        }

        public IIngredientBase SelectedIngredient
        {
            set
            {
                _returnValue = value;
                _window.Close();
            }
        }

        private void AddNewDefinitionAndReturnIt(string newIngredientName = "")
        {
            IIngredientBase ingredient = IngredientDefinitionViewModel.AddNewIngredientDefinition(newIngredientName);
            if (ingredient != null)
            {
                _returnValue = ingredient;
                _window.Close();
            }
        }

        public ICommand AddNewDefinitionCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    AddNewDefinitionAndReturnIt();
                });
            }
        }

        public ICommand EditIngredientDetails
        {
            get
            {
                return new RelayCommand<IIngredientBase>((delegate (IIngredientBase ingredient)
                {
                    IngredientDefinitionViewModel.EditIngredientDefinition(ingredient);
                }));
            }
        }

        public ICommand ClickedEnterCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (_view.MainListView.Items.Count > 0)
                    {
                        _returnValue = _view.MainListView.Items[0] as IIngredientBase;
                        _window.Close();
                    }
                    else
                    {
                        AddNewDefinitionAndReturnIt(_view.FilterTextBox.Text);
                    }
                });
            }
        }

    }
}
