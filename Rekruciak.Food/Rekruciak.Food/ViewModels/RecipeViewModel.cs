﻿using ITA.IO_ThirdParty;
using ITA.WPF_ThirdParty;
using ITA.WPF_ThirdParty.ShowDialogs;
using Rekruciak.Food.Interfaces.BO;
using Rekruciak.Food.Interfaces.BO.Ingredients;
using Rekruciak.Food.UI.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF_ThirdParty;
using WPF_ThirdParty.VM_Implementation;

namespace Rekruciak.Food.UI.ViewModels
{
    public class RecipeViewModel : BaseViewModel
    {
        public RecipeDetailsView View { get; internal set; }
        private readonly IRecipe _recipe;
        private readonly IRecipe _recipeBackup;
        private readonly bool _isCreatingNew;
        public RecipeViewModel(IRecipe recipe, bool isCreatingNew)
        {
            _recipe = recipe;
            _preparationSteps = new ObservableCollection<IPreparationStep>(_recipe.PreparationSteps);
            PreparationSteps.CollectionChanged += ((sender, e) => _recipe.PreparationSteps = PreparationSteps);
            _ingredients = new ObservableCollection<IIngredientBase>(_recipe.Ingredients);
            Ingredients.CollectionChanged += ((sender, e) => _recipe.Ingredients = Ingredients);
            _recipeBackup = ApplicationViewModel.Instance.BLC.CloneRecipeByValue(recipe);
            _isCreatingNew = isCreatingNew;
        }

        public string Name
        {
            get => _recipe.Name;
            set
            {
                _recipe.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private ObservableCollection<IPreparationStep> _preparationSteps;
        public ObservableCollection<IPreparationStep> PreparationSteps
        {
            get => _preparationSteps;
            set => _preparationSteps = value;
        }

        private ObservableCollection<IIngredientBase> _ingredients;
        public ObservableCollection<IIngredientBase> Ingredients
        {
            get => _ingredients;
            set => _ingredients = value;
        }

        public ICommand SaveAndExitCommand
        {
            get
            {
                return new RelayCommand(
                    () =>
                    {
                        ApplicationViewModel.Instance.BLC.SaveRecipe(_recipe);
                        ScreenController.Instance.SetWindowContent(new AllRecipesView());
                    },
                    () => Name.Any());
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (_isCreatingNew)
                        ApplicationViewModel.Instance.BLC.RemoveRecipe(_recipe);
                    else
                        ApplicationViewModel.Instance.BLC.RestoreRecipeBackup(_recipe, _recipeBackup);
                    ScreenController.Instance.SetWindowContent(new AllRecipesView());
                });
            }
        }
        public ICommand AddNewIngredient
        {
            get
            {
                return new RelayCommand(() =>
                {
                    IIngredientBase ingredient = IngredientsListViewModel.LetUserChooseNewIngredientToRecipe();
                    if (ingredient != null)
                        Ingredients.Add(ApplicationViewModel.Instance.BLC.CloneIngredientByValue(ingredient));
                });
            }
        }

        #region On ingredient clicked shortcuts

        public ICommand RemoveIngredientCommand
        {
            get
            {
                return new RelayCommand<IIngredientBase>(delegate (IIngredientBase ingredient)
                {
                    if (MessageDialog.AskQuestion($"Usunąć {ingredient.Name}?"))
                        Ingredients.Remove(ingredient);
                });
            }
        }
        #endregion
        
        public ICommand NameClickedEscapeCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (MessageDialog.AskQuestion($"Wyjść bez zapisu?"))
                        CancelCommand.Execute(null);
                });
            }
        }

        private void SetStepsLps()
        {
            for (int i = 1; i <= PreparationSteps.Count; i++)
                PreparationSteps[i - 1].Lp = i;
        }


        public ICommand AddNewStep
        {
            get
            {
                return new RelayCommand<IPreparationStep>(delegate (IPreparationStep previousStep)
                {
                    IPreparationStep preparationStep = ApplicationViewModel.Instance.BLC.CreateEmptyPreparationStep();
                    if (previousStep == null || PreparationSteps.IndexOf(previousStep) + 1 == PreparationSteps.Count)
                        PreparationSteps.Add(preparationStep); //Dodajemy na koniec
                    else
                        PreparationSteps.Insert(PreparationSteps.IndexOf(previousStep) + 1, preparationStep); //Dodajemy w środku
                    SetStepsLps();
                });
            }
        }

        #region On preparationstep clicked shortcuts
        public ICommand RemoveStepCommand // Escape
        {
            get
            {
                return new RelayCommand<IPreparationStep>(delegate (IPreparationStep step)
                {
                    if (MessageDialog.AskQuestion($"Usunąć {step.Description}?"))
                    {
                        PreparationSteps.Remove(step);
                        SetStepsLps();
                    }
                });
            }
        }
        #endregion

    }
}
