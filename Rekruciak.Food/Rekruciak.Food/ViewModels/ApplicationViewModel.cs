﻿using ITA.IO_ThirdParty;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_ThirdParty;

namespace Rekruciak.Food.UI.ViewModels
{
    public class ApplicationViewModel : BaseViewModel
    {
        public readonly BLC.BLC BLC;

        #region Init
        private ApplicationViewModel()
        {
            Properties.Settings sett = new Properties.Settings();
            string pathToSerializationFile = Path.Combine(Paths.Documents, "Food", sett.SerializationFileName);
            BLC = new BLC.BLC(sett.DbNameConf, pathToSerializationFile);
        }

        private static ApplicationViewModel _instance;
        public static ApplicationViewModel Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ApplicationViewModel();
                return _instance;
            }
        }
        #endregion
        
    }
}
