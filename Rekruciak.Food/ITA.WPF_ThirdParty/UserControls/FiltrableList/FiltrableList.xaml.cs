﻿using ITA.WPF_ReuseableUserControls.FiltrableList.Services;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using WPF_ThirdParty.Windows;

namespace ITA.WPF_ReuseableUserControls.FiltrableList
{
    /// <summary>
    /// Interaction logic for FiltrableList.xaml
    /// </summary>
    public partial class FiltrableList : UserControl
    {
        FilterService _filterService = new FilterService();
        ListViewContentGenerator _listViewContentGenerator;

        private UserSelectedItemEvent _eventHandler;
        private event UserSelectedItemEvent _userSelectedItem;
        public delegate void UserSelectedItemEvent(object selectedItem);

        private FiltrableList() { }

        public FiltrableList(object viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
            _listViewContentGenerator = new ListViewContentGenerator(MainListView);
            this.Loaded += FiltrableList_Loaded;
            Unloaded += ((s, e) =>
            {
                if (_eventHandler != null)
                    _userSelectedItem -= _eventHandler;
            });
        }

        public FiltrableList(object viewModel, UserSelectedItemEvent eventHandler) : this(viewModel)
        {
            _userSelectedItem += eventHandler;
            _eventHandler = eventHandler;
        }

        public FiltrableList(object viewModel, UserSelectedItemEvent eventHandler, string listHeaderTextBlock) : this(viewModel, eventHandler)
        {
            ListHeaderTextBlock.Text = listHeaderTextBlock;
        }

        public FiltrableList(object viewModel, UserSelectedItemEvent eventHandler, bool filtrVisible) : this(viewModel, eventHandler)
        {
            if (!filtrVisible)
                FiltrGrid.Visibility = Visibility.Collapsed;
        }

        public FiltrableList(object viewModel, string listHeaderTextBlock) : this(viewModel)
        {
            ListHeaderTextBlock.Text = listHeaderTextBlock;
        }

        private void FiltrableList_Loaded(object sender, RoutedEventArgs e)
        {
            _listViewContentGenerator.AdjustColumnsWidth();
        }

        private void TxtFilter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            _filterService.SetValueToFilterBy((sender as TextBox).Text);
            CollectionViewSource.GetDefaultView(MainListView.ItemsSource).Refresh();
        }

        private void ListView_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            var view = CollectionViewSource.GetDefaultView((sender as ListView).ItemsSource);
            if (view != null)
            {
                _listViewContentGenerator.SetListViewView();
                _filterService.SetPropertiesToFilterBy(sender as ItemsControl);
                view.Filter = _filterService.FrameworkFilter;
            }
        }

        private void FiltrableList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ListView).SelectedItems.Count > 0 && _userSelectedItem != null && _userSelectedItem.GetInvocationList().Count() > 0)
            {
                _userSelectedItem((sender as ListView).SelectedItem);
                (sender as ListView).UnselectAll();
            }
        }

        private void EdytujFiltrButton_Click(object sender, RoutedEventArgs e)
        {
            string userFilter = FullKeyboard.ShowKeyboard();
            if (userFilter != null)
                txtFilter.Text = userFilter;
        }

        private void WyczyscFiltrButton_Click(object sender, RoutedEventArgs e)
        {
            txtFilter.Text = "";
        }
    }
}
