﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ITA.WPF_ReuseableUserControls.FiltrableList.Services
{
    public class FilterService
    {
        private string _valueToFilterBy;
        private PropertyInfo[] _filtrableProps;

        public void SetValueToFilterBy(string valueToFilterBy)
        {
            _valueToFilterBy = valueToFilterBy;
        }

        public void SetPropertiesToFilterBy(System.Windows.Controls.ItemsControl itemsControl)
        {
            var type = itemsControl.ItemsSource.GetType();
            var props = type.GenericTypeArguments[0].GetProperties();
            _filtrableProps = (itemsControl.ItemsSource.GetType()).GenericTypeArguments[0].GetProperties()
                .Where(q => q.GetCustomAttribute(typeof(Filtrable)) != null).ToArray();
        }

        public bool FrameworkFilter(object item)
        {
            return String.IsNullOrEmpty(_valueToFilterBy) ? true :
                 _filtrableProps.Any(prop =>
                    item.GetType().GetProperty(prop.Name).GetValue(item, null)?.ToString() != null
                    && item.GetType().GetProperty(prop.Name).GetValue(item, null).ToString().IndexOf(_valueToFilterBy, StringComparison.OrdinalIgnoreCase) >= 0);
        }
    }
}
