﻿using ITA.WPF_ReuseableUserControls.FiltrableList.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ITA.WPF_ReuseableUserControls.FiltrableList.Services
{
    public class ListViewContentGenerator
    {
        private ListView _listView;

        public ListViewContentGenerator(ListView listView)
        {
            _listView = listView;
        }

        private string GetPropertyHeaderText(PropertyInfo prop)
        {
            var atr = prop.GetCustomAttribute(typeof(ColumnHeader)) as ColumnHeader;
            return atr != null ?
                atr.HeaderText != null ? atr.HeaderText : prop.Name : prop.Name;
        }

        public void SetListViewView()
        {
            PropertyInfo[] props = (_listView.ItemsSource.GetType()).GenericTypeArguments[0].GetProperties();

            GridView myGridView = new GridView()
            {
                AllowsColumnReorder = true,
                ColumnHeaderContainerStyle = Application.Current.FindResource("FiltrableListHeaderStyle") as Style
            };

            foreach (PropertyInfo prop in props)
            {
                VisibleOnList ds = prop.GetCustomAttribute(typeof(VisibleOnList)) as VisibleOnList;
                if (ds != null && ds.IsVisible)
                {
                    GridViewColumn gvc = new GridViewColumn()
                    {
                        //DisplayMemberBinding = new Binding(prop.Name),
                        Header = GetPropertyHeaderText(prop),
                        CellTemplate = getDataTemplate(prop.Name)
                    };
                    myGridView.Columns.Add(gvc);
                }
            }

            _listView.View = myGridView;
        }

        private DataTemplate getDataTemplate(string bindingPath)
        {
            DataTemplate template = new DataTemplate();
            FrameworkElementFactory factory = new FrameworkElementFactory(typeof(TextBlock));
            factory.SetValue(TextBlock.TextProperty, new Binding(bindingPath));
            factory.SetValue(TextBlock.StyleProperty, Application.Current.FindResource("FiltrableListTextblockStyle") as Style);
            template.VisualTree = factory;

            return template;
        }

        public void AdjustColumnsWidth()
        {
            if (_listView.View != null)
            {
                double columnsMaxWidth = _listView.ActualWidth - 85;
                foreach (GridViewColumn column in (_listView.View as GridView).Columns)
                    column.Width = columnsMaxWidth / (_listView.View as GridView).Columns.Count;
            }
        }
    }
}
