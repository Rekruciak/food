﻿using ITA.WPF_ReuseableUserControls.FiltrableList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_ThirdParty.Windows;
using static ITA.WPF_ReuseableUserControls.FiltrableList.FiltrableList;

namespace WPF_ThirdParty.ShowDialogs
{
    /// <summary>
    /// Interaction logic for sdChooseItemFromList.xaml
    /// </summary>
    public partial class sdChooseItemFromList : UserControl
    {
        private static ShowDialogWindow windowToShow;

        public sdChooseItemFromList(string title, object listViewModel, string listHeader)
        {
            InitializeComponent();
            TitleTextBlock.Text = title;
            ListContentControl.Content = new FiltrableList(listViewModel, choosedItemEven, listHeader);
        }

        private void choosedItemEven(object selectedItem)
        {
            returnValue = selectedItem;
            windowToShow.Close();
        }

        private object returnValue = null;

        public static object ShowWindow(string title, object listViewModel, string listHeader, ShowDialogWindow.SizeOptions sizeOptions)
        {
            sdChooseItemFromList windowContent = new sdChooseItemFromList(title, listViewModel, listHeader);

            windowToShow = new ShowDialogWindow(sizeOptions);
            windowToShow.SetWindowContent(windowContent);
            windowToShow.ShowDialog();

            return windowContent.returnValue;
        }

        public static object ShowWindow(string title, object listViewModel, string listHeader)
        {
            var sizeOptions = new ShowDialogWindow.SizeOptions(
                SystemParameters.WorkArea.Width * 0.6d,
                SystemParameters.WorkArea.Height * 0.6d
                );

            return ShowWindow(title, listViewModel, listHeader, sizeOptions);
        }

        public static object ShowWindow(string title, object listViewModel)
        {
            return ShowWindow(title, listViewModel, "");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            windowToShow.Close();
        }
    }
}
