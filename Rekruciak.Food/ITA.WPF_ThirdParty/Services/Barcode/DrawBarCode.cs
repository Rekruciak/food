﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_ThirdParty.Services.Barcode
{
    public static class DrawBarCode
    {
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DeleteObject(IntPtr value);

        public static void Draw_Code128(System.Windows.Controls.Image image, string code)
        {
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.Alignment = BarcodeLib.AlignmentPositions.CENTER;
            b.IncludeLabel = true;
            //b.BarWidth = (int)image.ActualWidth;// (int)image.ActualWidth;
            var resultImage = b.Encode(BarcodeLib.TYPE.CODE128, code.Trim(), (int)image.DesiredSize.Width, (int)image.DesiredSize.Height - (int)image.Margin.Top - (int)image.Margin.Bottom);
            //var resultImage = b.Encode(BarcodeLib.TYPE.CODE128, code);//.Trim(), 150, 60);

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(resultImage);
            IntPtr bmpPt = bmp.GetHbitmap();
            using (var ms = new MemoryStream())
            {
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ms.Position = 0;

                var bi = new BitmapImage();
                bi.BeginInit();
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.StreamSource = ms;
                bi.EndInit();
                image.Source = bi;
            }
            DeleteObject(bmpPt);

        }

        public static void Draw_Code39(System.Windows.Controls.Image image, string code)
        {
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.Alignment = BarcodeLib.AlignmentPositions.CENTER;
            b.IncludeLabel = true;
            //b.BarWidth = (int)image.ActualWidth;// (int)image.ActualWidth;
            var resultImage = b.Encode(BarcodeLib.TYPE.CODE39Extended, code.Trim(), (int)image.DesiredSize.Width, (int)image.DesiredSize.Height - (int)image.Margin.Top - (int)image.Margin.Bottom);
            //var resultImage = b.Encode(BarcodeLib.TYPE.CODE128, code);//.Trim(), 150, 60);

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(resultImage);
            IntPtr bmpPt = bmp.GetHbitmap();
            using (var ms = new MemoryStream())
            {
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ms.Position = 0;

                var bi = new BitmapImage();
                bi.BeginInit();
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.StreamSource = ms;
                bi.EndInit();
                image.Source = bi;
            }
            DeleteObject(bmpPt);
        }

        public static void Draw_Code39(Canvas canvas, string code, bool checkDigit, bool readableText)
        {
            /////////////////////////////////////
            // Encode The Data
            /////////////////////////////////////
            Barcode bb = new Barcode();
            bb.BarcodeType = Barcode.BarcodeEnum.Code39;
            bb.Data = code;
            bb.CheckDigit = checkDigit ? Barcode.YesNoEnum.Yes : Barcode.YesNoEnum.No;
            bb.encode();

            int thinWidth;
            int thickWidth;

            thinWidth = 3;
            thickWidth = 3 * thinWidth;

            string outputString = bb.EncodedData;
            string humanText = bb.HumanText;


            /////////////////////////////////////
            // Draw The Barcode
            /////////////////////////////////////
            int len = outputString.Length;
            int currentPos = 10;
            int currentTop = 10;
            int currentColor = 0;
            for (int i = 0; i < len; i++)
            {
                Rectangle rect = new Rectangle();
                rect.Height = canvas.ActualHeight;
                if (currentColor == 0)
                {
                    currentColor = 1;
                    rect.Fill = new SolidColorBrush(Colors.Black);

                }
                else
                {
                    currentColor = 0;
                    rect.Fill = new SolidColorBrush(Colors.White);

                }
                Canvas.SetLeft(rect, currentPos);
                Canvas.SetTop(rect, currentTop);

                if (outputString[i] == 't')
                {
                    rect.Width = thinWidth;
                    currentPos += thinWidth;

                }
                else if (outputString[i] == 'w')
                {
                    rect.Width = thickWidth;
                    currentPos += thickWidth;

                }
                canvas.Children.Add(rect);

            }

            if (readableText)
            {
                /////////////////////////////////////
                // Add the Human Readable Text
                /////////////////////////////////////
                TextBlock tb = new TextBlock();
                tb.Text = humanText;
                tb.FontSize = 24;
                tb.FontFamily = new FontFamily("Courier New");
                Rect rx = new Rect(0, 0, 0, 0);
                tb.Arrange(rx);
                Canvas.SetLeft(tb, (currentPos - tb.ActualWidth) / 2);
                Canvas.SetTop(tb, currentTop + canvas.ActualHeight + 5);
                canvas.Children.Add(tb);
            }
        }
    }
}
