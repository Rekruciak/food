﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_ThirdParty.Windows
{
    /// <summary>
    /// Interaction logic for Calculator.xaml
    /// </summary>
    public partial class Calculator : Window, INotifyPropertyChanged
    {
        public Calculator()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += Calculator_Loaded;
        }

        private void Calculator_Loaded(object sender, RoutedEventArgs e)
        {
            ValueTextBox.Focus();
        }

        DataTable dt = new DataTable();

        public static string ShowCalculator(string value)
        {
            Calculator calc = new Calculator();
            calc.Amount = value;
            calc.ShowDialog();
            return ret;
        }

        public static string ShowCalculator()
        {
            return ShowCalculator("");
        }

        private static string ret;

        public enum Operation
        {
            Add, Subtract, Multiple, Divide
        }

        private Operation? _operation = null;

        private string _amount { get; set; }
        public string Amount
        {
            get { return _amount; }
            set
            {
                _amount = value; OnPropertyChanged("Amount");
            }
        }

        #region ButtonsEvents
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Amount = "";
            _operation = null;
        }

        private void UndoButton_Click(object sender, RoutedEventArgs e)
        {
            if (Amount.Length > 0)
            {
                if (Amount.Last().Equals('+') || Amount.Last().Equals('-') || Amount.Last().Equals('*') || Amount.Last().Equals('/'))
                    _operation = null;
                Amount = Amount.Substring(0, Amount.Length - 1);
            }
        }

        private void DivideButton_Click(object sender, RoutedEventArgs e)
        {
            if (Amount.Length > 0)
            {
                if (!(Amount.Last().Equals('+') || Amount.Last().Equals('-') || Amount.Last().Equals('*') || Amount.Last().Equals('/')))
                {
                    if (_operation == null)
                    {
                        _operation = Operation.Divide;
                        Amount += "/";
                    }
                    else
                    {
                        EqualButton_Click(sender, e);
                        _operation = Operation.Divide;
                        Amount += "/";
                    }
                }
                else
                {
                    Amount = Amount.Substring(0, Amount.Length - 1) + "/";
                }
            }
        }

        private void MultipleButton_Click(object sender, RoutedEventArgs e)
        {
            if (Amount.Length > 0)
            {
                if (!(Amount.Last().Equals('+') || Amount.Last().Equals('-') || Amount.Last().Equals('*') || Amount.Last().Equals('/')))
                {
                    if (_operation == null)
                    {
                        _operation = Operation.Multiple;
                        Amount += "*";
                    }
                    else
                    {
                        EqualButton_Click(sender, e);
                        _operation = Operation.Multiple;
                        Amount += "*";
                    }
                }
                else
                {
                    Amount = Amount.Substring(0, Amount.Length - 1) + "*";
                }
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if (Amount.Length > 0)
            {
                if (!(Amount.Last().Equals('+') || Amount.Last().Equals('-') || Amount.Last().Equals('*') || Amount.Last().Equals('/')))
                {
                    if (_operation == null)
                    {
                        _operation = Operation.Add;
                        Amount += "+";
                    }
                    else
                    {
                        EqualButton_Click(sender, e);
                        _operation = Operation.Add;
                        Amount += "+";
                    }
                }
                else
                {
                    Amount = Amount.Substring(0, Amount.Length - 1) + "+";
                }
            }
        }

        private void MinusButton_Click(object sender, RoutedEventArgs e)
        {
            if (Amount.Length > 0)
            {
                if (!(Amount.Last().Equals('+') || Amount.Last().Equals('-') || Amount.Last().Equals('*') || Amount.Last().Equals('/')))
                {
                    if (_operation == null)
                    {
                        _operation = Operation.Subtract;
                        Amount += "-";
                    }
                    else
                    {
                        EqualButton_Click(sender, e);
                        _operation = Operation.Subtract;
                        Amount += "-";
                    }
                }
                else
                {
                    Amount = Amount.Substring(0, Amount.Length - 1) + "-";
                }
            }
        }

        private void Button_1_Click(object sender, RoutedEventArgs e)
        {
            Amount += "1";
        }

        private void Button_2_Click(object sender, RoutedEventArgs e)
        {
            Amount += "2";
        }

        private void Button_3_Click(object sender, RoutedEventArgs e)
        {
            Amount += "3";
        }

        private void Button_4_Click(object sender, RoutedEventArgs e)
        {
            Amount += "4";
        }

        private void Button_5_Click(object sender, RoutedEventArgs e)
        {
            Amount += "5";
        }

        private void Equal()
        {
            try
            {
                if (Amount.Length > 0 && !(Amount.Last().Equals('+') || Amount.Last().Equals('-') || Amount.Last().Equals('*') || Amount.Last().Equals('/')))
                {
                    Amount = dt.Compute(Amount.Replace(',', '.'), "").ToString();
                    _operation = null;
                }
            }
            catch
            {
                Amount = "";
            }
        }

        private void EqualButton_Click(object sender, RoutedEventArgs e)
        {
            Equal();
        }

        private void Button_6_Click(object sender, RoutedEventArgs e)
        {
            Amount += "6";
        }

        private void Button_7_Click(object sender, RoutedEventArgs e)
        {
            Amount += "7";
        }

        private void Button_8_Click(object sender, RoutedEventArgs e)
        {
            Amount += "8";
        }

        private void Button_9_Click(object sender, RoutedEventArgs e)
        {
            Amount += "9";
        }

        private void Button_0_Click(object sender, RoutedEventArgs e)
        {
            Amount += "0";
        }

        private void CommaButton_Click(object sender, RoutedEventArgs e)
        {
            if (Amount.IndexOf(',') == -1)
            {
                if (Amount.Length > 0)
                    Amount += ",";
                else
                    Amount += "0,";
            }
            else
            {
                if (_operation != null)
                {
                    switch (_operation)
                    {
                        case Operation.Add:
                            if (Amount.Substring(Amount.IndexOf('+')).Count(x => x == ',') == 0)
                            {
                                if (Amount.Substring(Amount.IndexOf('+')).Length > 1)
                                    Amount += ",";
                                else
                                    Amount += "0,";
                            }
                            break;
                        case Operation.Divide:
                            if (Amount.Substring(Amount.IndexOf('/')).Count(x => x == ',') == 0)
                            {
                                if (Amount.Substring(Amount.IndexOf('/')).Length > 1)
                                    Amount += ",";
                                else
                                    Amount += "0,";
                            }
                            break;
                        case Operation.Multiple:
                            if (Amount.Substring(Amount.IndexOf('*')).Count(x => x == ',') == 0)
                            {
                                if (Amount.Substring(Amount.IndexOf('*')).Length > 1)
                                    Amount += ",";
                                else
                                    Amount += "0,";
                            }
                            break;
                        case Operation.Subtract:
                            if (Amount.Substring(Amount.IndexOf('-')).Count(x => x == ',') == 0)
                            {
                                if (Amount.Substring(Amount.IndexOf('-')).Length > 1)
                                    Amount += ",";
                                else
                                    Amount += "0,";
                            }
                            break;
                    }
                }
            }
        }
        #endregion

        private void AnulujButton_Click(object sender, RoutedEventArgs e)
        {
            ret = null;
            this.Close();
        }

        private void Potwierdz()
        {
            if (_operation != null)
                Equal();//EqualButton_Click(sender, e);
            else
            {
                Equal();//EqualButton_Click(sender, e);
                ret = Amount;
                this.Close();
            }
        }

        private void PotwierdzButton_Click(object sender, RoutedEventArgs e)
        {
            Potwierdz();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ValueTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            if (e.Key == Key.Enter)
            {
                Potwierdz();
            }
            if (e.Key >= Key.D0 && e.Key <= Key.D9)
            {
                Amount += e.Key.ToString().Substring(1);
                ValueTextBox.Select(ValueTextBox.Text.Length, 0);
            }
            if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                Amount += e.Key.ToString().Substring(6);
                ValueTextBox.Select(ValueTextBox.Text.Length, 0);
            }
            if (e.Key == Key.Back)
            {
                if (Amount.Length > 0)
                    Amount = Amount.Substring(0, Amount.Length - 1);
                ValueTextBox.Select(ValueTextBox.Text.Length, 0);
            }
        }
    }
}
