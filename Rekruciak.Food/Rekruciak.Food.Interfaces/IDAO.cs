﻿using Rekruciak.Food.Interfaces.BO;
using Rekruciak.Food.Interfaces.BO.Ingredients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekruciak.Food.Interfaces
{
    public interface IDAO
    {
        IEnumerable<IRecipe> GetAllRecipes();

        IRecipe CreateEmptyRecipe();
        void SaveRecipe(IRecipe recipe);
        void RemoveRecipe(IRecipe recipe);


        // Ingredients definition list 
        IEnumerable<IIngredientBase> GetAllIngredients();

        ILiquidIngredient CreateEmptyLiquidIngredient();
        IStableIngredient CreateEmptyStableIngredient();
        ICountableIngredient CreateEmptyCountableIngredient();
        void SaveIngredientDefinition(IIngredientBase ingredientBase);
        void RemoveIngredientDefinition(IIngredientBase ingredientBase);
        IRecipe CloneRecipeByValue(IRecipe recipe);
        IIngredientBase CloneIngredientByValue(IIngredientBase ingredient);
        void RestoreRecipeBackup(IRecipe recipeToRestore, IRecipe recipeBackup);
        //void AddNewPreparationStep(IRecipe recipe, int? newStepIndex);
        IPreparationStep CreateEmptyPreparationStep();

        // Ingredients of the recipe list
        // is supported by modifing Recipe object and calling
        // SaveRecipe method
        // Adding new ingredient to recipe is supported by clone by value 
    }
}
