﻿using ITA.WPF_ReuseableUserControls;
using Rekruciak.Food.Interfaces.BO.Ingredients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Rekruciak.Food.Interfaces.BO
{
    public interface IRecipe
    {
        [Filtrable]
        string Name { get; set; }
        IEnumerable<IPreparationStep> PreparationSteps { get; set; }
        IEnumerable<IIngredientBase> Ingredients { get; set; }
        decimal Kcal { get; }

    }
}
