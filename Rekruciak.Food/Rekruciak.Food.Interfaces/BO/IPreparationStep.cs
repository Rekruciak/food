﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekruciak.Food.Interfaces.BO
{
    public interface IPreparationStep
    {
        string Description { get; set; }
        int Lp { get; set; }
    }
}
