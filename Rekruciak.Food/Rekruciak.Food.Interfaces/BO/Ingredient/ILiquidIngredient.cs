﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekruciak.Food.Interfaces.BO.Ingredients
{
    public interface ILiquidIngredient : IUncountableIngredient
    {
        decimal Density { get; set; } // [g/ml]
        decimal Volume { get; set; } // [ml]
    }
}
