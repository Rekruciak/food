﻿using ITA.WPF_ReuseableUserControls;
using Rekruciak.Food.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekruciak.Food.Interfaces.BO.Ingredients
{
    public interface IIngredientBase
    {
        [Filtrable]
        string Name { get; set; }
        decimal Weight { get; set; }
        string Unit { get; }

        decimal CarbsIn100g { get; set; }
        decimal FiberIn100g { get; set; }
        decimal FatIn100g { get; set; }
        decimal ProteinIn100g { get; set; }
        decimal KcalPer100g { get; }
        decimal Kcal { get; }
        IngredientType Type { get; set; }
    }
}
