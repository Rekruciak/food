﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekruciak.Food.Interfaces.BO.Ingredients
{
    public interface ICountableIngredient : IIngredientBase
    {
        int Amount { get; set; }
        decimal WeightPerPiece { get; set; }
    }
}
