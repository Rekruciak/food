﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ITA.IO_ThirdParty
{
    public class Serialization
    {
        public static T Deserialize<T>(string xml)
        {
            var doc = new XmlDocument();
            var XML_serializer = new XmlSerializer(typeof(T));
            doc.LoadXml(xml);
            using (var nodeReader = new XmlNodeReader(doc))
                return (T)XML_serializer.Deserialize(nodeReader);
        }

        public static string Serialize<T>(T objectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            using (var sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww))
            {
                xmlSerializer.Serialize(writer, objectToSerialize);
                return sww.ToString();
            }
        }

        public static T CloneObjectByValueWithSerialization<T>(T objectToClone)
        {
            return Deserialize<T>(Serialize(objectToClone));
        }
    }
}
