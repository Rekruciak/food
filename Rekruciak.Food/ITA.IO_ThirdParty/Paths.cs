﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Environment;

namespace ITA.IO_ThirdParty
{
    public class Paths
    {
        public static string Desktop
        {
            get
            {
                return GetFolderPath(SpecialFolder.Desktop);
            }
        }

        public static string Documents
        {
            get
            {
                return GetFolderPath(SpecialFolder.MyDocuments);
            }
        }
    }
}
