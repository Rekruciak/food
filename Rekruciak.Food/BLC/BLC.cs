﻿using Rekruciak.Food.Interfaces;
using Rekruciak.Food.Interfaces.BO;
using Rekruciak.Food.Interfaces.BO.Ingredients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Rekruciak.Food.BLC
{
    public class BLC
    {
        private readonly IDAO _dao;

        public BLC(string dbName, string fileFullPathToSerialize)
        {
            if (!dbName.ToUpper().EndsWith(".DLL"))
                dbName = dbName + ".dll";
            if (!fileFullPathToSerialize.ToUpper().EndsWith(".XML"))
                fileFullPathToSerialize = fileFullPathToSerialize + ".xml";
            Type daoToCreate = Assembly.UnsafeLoadFrom(dbName)
                .GetTypes()
                .FirstOrDefault(q => q.GetInterfaces()
                .Any(p => p == typeof(IDAO)));

            _dao = (IDAO)Activator.CreateInstance(daoToCreate, new object[] { fileFullPathToSerialize });
        }

        public void RestoreRecipeBackup(IRecipe recipeToRestore, IRecipe recipeBackup)
        {
            _dao.RestoreRecipeBackup(recipeToRestore, recipeBackup);
        }

        public IEnumerable<IRecipe> GetAllRecipes()
        {
            return _dao.GetAllRecipes();
        }

        public IRecipe CreateEmptyRecipe()
        {
            return _dao.CreateEmptyRecipe();
        }

        public IRecipe CloneRecipeByValue(IRecipe recipe)
        {
            return _dao.CloneRecipeByValue(recipe);
        }

        public IIngredientBase CloneIngredientByValue(IIngredientBase ingredient)
        {
            return _dao.CloneIngredientByValue(ingredient);
        }

        public IIngredientBase CreateEmptyIngredient<T>() where T : IIngredientBase
        {
            if (typeof(T) == typeof(ICountableIngredient))
                return _dao.CreateEmptyCountableIngredient();
            if (typeof(T) == typeof(ILiquidIngredient))
                return _dao.CreateEmptyLiquidIngredient();
            if (typeof(T) == typeof(IStableIngredient))
                return _dao.CreateEmptyStableIngredient();
            throw new NotImplementedException($@"CreateEmptyIngredient - not supported type: {typeof(T).ToString()}");
        }

        public void SaveRecipe(IRecipe recipe)
        {
            _dao.SaveRecipe(recipe);
        }

        public void RemoveRecipe(IRecipe recipe)
        {
            _dao.RemoveRecipe(recipe);
        }

        public void SaveIngredientDefinition(IIngredientBase ingredientBase)
        {
            _dao.SaveIngredientDefinition(ingredientBase);
        }

        //public void AddNewPreparationStep(IRecipe recipe, int? newStepIndex)
        //{
        //    _dao.AddNewPreparationStep(recipe, newStepIndex);
        //}

        public IPreparationStep CreateEmptyPreparationStep()
        {
            return _dao.CreateEmptyPreparationStep();
        }

        public IEnumerable<IIngredientBase> GetAllIngredientsDefinitions()
        {
            return _dao.GetAllIngredients();
        }
    }
}
