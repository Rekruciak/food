﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekruciak.Food.Core
{
    public enum CountableIngredientUnitType
    {
        [Description("Łyżka")]
        TableSpoon,

        [Description("Łyżeczka")]
        Spoon,

        [Description("szt.")]
        Piece,
    }
}
