﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekruciak.Food.Core
{
    public enum IngredientType
    {
        Meat,
        Frozen,
        Dairy,
        Cheese,
        Vegatable,
        Fruit,
        Carbons,
        Canned,
        Fish,
        Spice,
        Sauce,
        Liquid
    }
}
