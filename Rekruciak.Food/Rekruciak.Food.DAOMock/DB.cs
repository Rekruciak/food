﻿using ITA.IO_ThirdParty;
using Rekruciak.Food.DAOMock.BO;
using Rekruciak.Food.DAOMock.BO.Ingredient;
using Rekruciak.Food.Interfaces;
using Rekruciak.Food.Interfaces.BO;
using Rekruciak.Food.Interfaces.BO.Ingredients;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Rekruciak.Food.DAOMock
{
    public class DB : IDAO
    {
        private readonly string _serializationFileFullPath;
        public DB(string serializationFileFullPath)
        {
            _serializationFileFullPath = serializationFileFullPath;
            IfPossibleDeserializeDataFromFile();
        }

        private DataSerializationObject _dataSerializationObject { get; set; }

        public ICountableIngredient CreateEmptyCountableIngredient()
        {
            return new CountableIngredient();
        }

        public ILiquidIngredient CreateEmptyLiquidIngredient()
        {
            return new LiquidIngredient();
        }

        public IStableIngredient CreateEmptyStableIngredient()
        {
            return new StableIngredient();
        }

        public IRecipe CreateEmptyRecipe()
        {
            Recipe recipe = new Recipe();
            _dataSerializationObject.Recipes.Add(recipe);
            return recipe;
        }

        public IEnumerable<IIngredientBase> GetAllIngredients()
        {
            return _dataSerializationObject.Ingredients.Any()
                ? _dataSerializationObject.Ingredients
                : new List<IngredientBase>
                {
                    new StableIngredient
                    {
                        Name = "Pomidor",
                        CarbsIn100g = 2.9m,
                        FatIn100g = 0.2m,
                        ProteinIn100g = 0.9m,
                        FiberIn100g = 1.2m
                    }
                };
        }

        public IEnumerable<IRecipe> GetAllRecipes()
        {
            return _dataSerializationObject.Recipes;
        }

        public void SaveIngredientDefinition(IIngredientBase ingredientBase)
        {
            // Normalnei by szedł insert / update do bazy
            if (!_dataSerializationObject.Ingredients.Contains(ingredientBase))
                _dataSerializationObject.Ingredients.Add(ingredientBase as IngredientBase);
            SerializeDataToFile();
        }

        public void SaveRecipe(IRecipe recipe)
        {
            // Normalnei by szedł insert / update do bazy
            SerializeDataToFile();
        }

        private void IfPossibleDeserializeDataFromFile()
        {
            try
            {
                _dataSerializationObject = DirectoriesFilesOperations.DoesFileExists(_serializationFileFullPath)
                    ? Serialization.Deserialize<DataSerializationObject>
                        (DirectoriesFilesOperations.GetFileContent(_serializationFileFullPath))
                    : new DataSerializationObject();
            }
            catch(Exception ex)
            {
                MessageBox.Show($@"Deserializacja pliku się nie powiodła
{ex.Message}");
                throw;
            }
        }

        private void SerializeDataToFile()
        {
            DirectoriesFilesOperations.CreateDirectoryIfDoesntExists(Path.GetDirectoryName(_serializationFileFullPath));
            DirectoriesFilesOperations.MakeFileWithSpecificContent
                (_serializationFileFullPath, Serialization.Serialize<DataSerializationObject>(_dataSerializationObject));
        }

        public void RemoveRecipe(IRecipe recipe)
        {
            _dataSerializationObject.Recipes.Remove(recipe as Recipe);
            SerializeDataToFile();
        }

        public void RemoveIngredientDefinition(IIngredientBase ingredientBase)
        {
            _dataSerializationObject.Ingredients.Remove(ingredientBase as IngredientBase);
            SerializeDataToFile();
        }

        public IRecipe CloneRecipeByValue(IRecipe recipe)
        {
            return Serialization.CloneObjectByValueWithSerialization(recipe as Recipe);
        }

        public IIngredientBase CloneIngredientByValue(IIngredientBase ingredient)
        {
            return Serialization.CloneObjectByValueWithSerialization(ingredient as IngredientBase);
        }

        public void RestoreRecipeBackup(IRecipe recipeToRestore, IRecipe recipeBackup)
        {
            if (_dataSerializationObject.Recipes.Contains(recipeToRestore))
                _dataSerializationObject.Recipes[_dataSerializationObject.Recipes.IndexOf(recipeToRestore as Recipe)] = recipeBackup as Recipe;
        }

        public IPreparationStep CreateEmptyPreparationStep()
        {
            return new PreparationStep();
        }

        //public void AddNewPreparationStep(IRecipe recipe, int? newStepIndex)
        //{
        //    _dataSerializationObject.Recipes[_dataSerializationObject.Recipes.IndexOf(recipe as Recipe)].
        //    if (newStepIndex != null)
        //        recipe.PreparationSteps.Add
        //}
    }
}
