﻿using ITA.WPF_ReuseableUserControls;
using Rekruciak.Food.DAOMock.BO.Ingredient;
using Rekruciak.Food.Interfaces.BO;
using Rekruciak.Food.Interfaces.BO.Ingredients;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Rekruciak.Food.DAOMock.BO
{
    public class Recipe : IRecipe
    {
        public Recipe()
        {
            Name = "";
            PreparationSteps = new List<PreparationStep>();
            Ingredients = new List<IngredientBase>();
        }
        public string Name { get; set; }
        
        private List<PreparationStep> _preparationSteps { get; set; }
        [XmlIgnore()]
        public IEnumerable<IPreparationStep> PreparationSteps
        {
            get { return _preparationSteps; }
            set
            {
                _preparationSteps = value.ToList().ConvertAll(q => (PreparationStep)q);
            }
        }

        private List<IngredientBase> _ingredients { get; set; }
        [XmlIgnore()]
        public IEnumerable<IIngredientBase> Ingredients
        {
            get { return _ingredients; }
            set
            {
                _ingredients = value.ToList().ConvertAll(q => (IngredientBase)q);
            }
        }
        
        public List<PreparationStep> Serializable_PreparationSteps
        {
            get => _preparationSteps;
            set => _preparationSteps = value;
        }

        public List<IngredientBase> Serializable_Ingredients
        {
            get => _ingredients;
            set => _ingredients = value;
        }

        public decimal Kcal => _ingredients.Sum(q => q.Kcal);
    }
}
