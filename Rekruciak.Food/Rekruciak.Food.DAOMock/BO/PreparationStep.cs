﻿using Rekruciak.Food.Interfaces.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_ThirdParty;

namespace Rekruciak.Food.DAOMock.BO
{
    public class PreparationStep : BaseViewModel, IPreparationStep
    {
        public PreparationStep()
        {
            Description = "";
        }

        public string Description { get; set; }

        private int _lp;
        public int Lp
        {
            get => _lp;
            set
            {
                _lp = value;
                OnPropertyChanged(nameof(Lp));
            }
        }

    }
}
