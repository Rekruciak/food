﻿using Rekruciak.Food.DAOMock.BO.Ingredient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Rekruciak.Food.DAOMock.BO
{
    [XmlInclude(typeof(CountableIngredient))]
    [XmlInclude(typeof(LiquidIngredient))]
    [XmlInclude(typeof(StableIngredient))]
    public class DataSerializationObject
    {
        public DataSerializationObject()
        {
            Recipes = new List<Recipe>();
            Ingredients = new List<IngredientBase>();
        }

        public List<Recipe> Recipes { get; set; }

        public List<IngredientBase> Ingredients { get; set; }
    }
}
