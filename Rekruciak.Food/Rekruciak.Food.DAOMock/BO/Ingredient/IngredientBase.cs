﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ITA.WPF_ReuseableUserControls;
using Rekruciak.Food.Core;
using Rekruciak.Food.Interfaces.BO.Ingredients;
using WPF_ThirdParty;

namespace Rekruciak.Food.DAOMock.BO.Ingredient
{
    [XmlInclude(typeof(CountableIngredient))]
    [XmlInclude(typeof(LiquidIngredient))]
    [XmlInclude(typeof(StableIngredient))]
    public abstract class IngredientBase : BaseViewModel, IIngredientBase
    {
        public decimal KcalPer1gOfCarbs { get { return 4; } }
        public decimal KcalPer1gOfProtein { get { return 4; } }
        public decimal KcalPer1gOfFat { get { return 9; } }

        public string Name { get; set; }
        private decimal _weight { get; set; }
        public decimal Weight
        {
            get => _weight;
            set
            {
                _weight = value;
                OnPropertyChanged(nameof(Weight));
                OnPropertyChanged(nameof(Carbs));
                OnPropertyChanged(nameof(Fat));
                OnPropertyChanged(nameof(Proteins));
                OnPropertyChanged(nameof(Kcal));
            }
        }
        public string Unit { get; set; }
        
        private bool IngredientCompositionValidation_IsValid(decimal newValue, decimal oldValue)
        {
            return
                CarbsIn100g +
                FiberIn100g +
                FatIn100g +
                ProteinIn100g -
                oldValue +
                newValue <= 100;
        }

        private decimal _carbsIn100g;
        public decimal CarbsIn100g
        {
            get => _carbsIn100g;
            set
            {
                if (IngredientCompositionValidation_IsValid(value, _carbsIn100g))
                    _carbsIn100g = value;
            }
        }
        private decimal _fiberIn100g;
        public decimal FiberIn100g
        {
            get => _fiberIn100g;
            set
            {
                if (IngredientCompositionValidation_IsValid(value, _fiberIn100g))
                    _fiberIn100g = value;
            }
        }
        private decimal _fatIn100g;
        public decimal FatIn100g
        {
            get => _fatIn100g;
            set
            {
                if (IngredientCompositionValidation_IsValid(value, _fatIn100g))
                    _fatIn100g = value;
            }
        }
        private decimal _proteinIn100g;
        public decimal ProteinIn100g
        {
            get => _proteinIn100g;
            set
            {
                if (IngredientCompositionValidation_IsValid(value, _proteinIn100g))
                    _proteinIn100g = value;
            }
        }

        public decimal Proteins
        {
            get => Decimal.Round(ProteinIn100g * Weight / 100m, 2);
        }

        public decimal Fat
        {
            get => Decimal.Round(FatIn100g * Weight / 100m, 2);
        }

        public decimal Carbs
        {
            get => Decimal.Round(CarbsIn100g * Weight / 100m, 2);
        }

        public decimal KcalPer100g
        {
            get => Decimal.Round(CarbsIn100g * KcalPer1gOfCarbs +
                    FatIn100g * KcalPer1gOfFat +
                    ProteinIn100g * KcalPer1gOfProtein, 2);
        }

        public decimal Kcal
        {
            get => Decimal.Round(KcalPer100g * Weight / 100m, 2);
        }

        public IngredientType Type { get; set; }
    }
}
