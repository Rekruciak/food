﻿using Rekruciak.Food.Interfaces.BO.Ingredients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekruciak.Food.DAOMock.BO.Ingredient
{
    public class CountableIngredient : IngredientBase, ICountableIngredient
    {
        public new string Unit { get; set; } = "szt.";

        private int _amount;
        public int Amount
        {
            get => _amount;
            set
            {
                if (value < 0)
                    return;

                _amount = value;
                Weight = WeightPerPiece * Amount;
            }
        }
        private decimal _weightPerPiece { get; set; }
        public decimal WeightPerPiece
        {
            get => _weightPerPiece;
            set
            {
                _weightPerPiece = value;
                Amount = Amount;    
            }
        }
    }
}
