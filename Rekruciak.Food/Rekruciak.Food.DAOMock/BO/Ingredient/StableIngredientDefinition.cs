﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rekruciak.Food.Interfaces.BO.Ingredients;

namespace Rekruciak.Food.DAOMock.BO.Ingredient
{
    public class StableIngredient : UncountableIngredient, IStableIngredient
    {
        public new string Unit { get; } = "g";
    }
}
