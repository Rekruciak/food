﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rekruciak.Food.Interfaces.BO.Ingredients;

namespace Rekruciak.Food.DAOMock.BO.Ingredient
{
    public class LiquidIngredient : UncountableIngredient, ILiquidIngredient
    {
        public new string Unit { get; } = "ml";

        private decimal _volume;
        public decimal Volume
        {
            get => _volume;
            set
            {
                if (_volume < 0)
                    return;

                _volume = value;
                Weight = Density * value;
            }
        }

        private decimal _density;
        public decimal Density
        {
            get => _density;
            set
            {
                _density = value;
                Volume = Volume;
            }
        }
    }
}
